#!/bin/bash
# configure libcurl as statically linked, minimalistics HTTP and HTTPS only lib
#
# 1) get libcurl from https://github.com/bagder/curl
# 2) copy this file to the curl directory
# 3) in the curl directory:
#    - run ./buildconf, then oryol_curl_config_https.sh (instead of ./configure), then make
#    - the compiled static lib is curl/lib/.libs/libcurl.a, copy this to oryol/libs/linux
#

#LIBS="-ldl -lpthread" ./configure --disable-shared --with-ssl=$HOME/build-openssl

#does not work. link error for the oryol application
#quite possible that we need the openssl.a libs as well at link time
#on the command line and on the link path
#-L and -l (unless I ship the openssl libs together with the libcurl libs)
#also, do i really need pthreads for openssl or curl? I'd rather like not to use it.




LIBS="-ldl -lpthread" ./configure --disable-shared --enable-http --disable-ftp --disable-file --disable-ldap --disable-rtsp --disable-dict --disable-telnet --disable-tftp --disable-pop3 --disable-imap --disable-smtp --disable-gopher --disable-manual --disable-sspi --disable-crypto-auth --disable-ntlm-wb --disable-tls-srp --disable-cookies --without-gnutls --without-polarssl --without-cyassl --without-nss --without-axtls --without-ca-path --without-libmetalink --without-libssh2 --without-librtmp --without-winidn --without-libidn --without-nghttp2 --disable-smb --disable-proxy --with-ssl=$HOME/dev/build-openssl/build
