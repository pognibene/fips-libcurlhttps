### CURL headers 

libcurl is used in Oryol as HTTP client on 64-bit Linux platforms. To prevent any compatibility problems with
different Linux configs it is linked statically into the executable. This version includes the HTTPS protocol
and is linked with OpenSSL.

*DISCLAIMER*
Only the Linux x64 version is compiled for now. Android, Ios, OS-X and Windows version will follow
once I have the time to produce them.


Build instructions for the Linux x64 version:

libcurl.a has been created like this:

1. git clone https://github.com/bagder/curl.git
2. cd curl
3. ./buildconfig (+ install any missing packages it complains about). Make sure you install libssl-dev on
debian/ubuntu before calling the script, otherwise curl will be compiled without HTTPS support.
4. copy the oryol_curl_config_https.sh script to the curl directory
5. run oryol_curl_config_https.sh instead of ./configure
6. make
7. the resulting lib is curl/lib/.libs/libcurl.a, copy this to oryol/libs/linux

